package searcher;

import java.util.List;

public interface PageSearcher<RT> {
    List<RT> search(String s, int page) throws Exception;
}
