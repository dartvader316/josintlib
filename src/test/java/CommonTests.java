import org.junit.jupiter.api.Assertions;
import search.Search;
import searchHelper.SearchHelper;
import searcher.*;

import java.util.HashSet;
import java.util.Set;

public class CommonTests {

    public static void searchTest(SearchHelper searchhelper) throws Exception {

        String search = "hello";

        GoogleSearcher gsearcher = new GoogleSearcher(searchhelper);
        gsearcher.setSearchSize(100);

        GoogleNewsSearcher gnsearcher = new GoogleNewsSearcher(searchhelper);
        gnsearcher.setSearchSize(100);

        GoogleRssNewsSearcher gnrsssearcher = new GoogleRssNewsSearcher(searchhelper);
        gnrsssearcher.setLanguage(GoogleRssNewsSearcher.googleNewsLanguage.RUSSIAN);
        gnrsssearcher.setSearchSize(100);

        YandexSearcher ysearcher = new YandexSearcher(searchhelper);
        ysearcher.setSearchSize(YandexSearcher.YANDEX_SEARCH_SUPPORTED_SIZE.SIZE_50);
        YandexNewsSearcher ynsearcher = new YandexNewsSearcher(searchhelper);

        GoogleCompanySearcher gcsearcher = new GoogleCompanySearcher(searchhelper);
        gcsearcher.setSearchSize(100);

        Set<Search> list = new HashSet<>();

        for (int i = 0; i < 1; i++) {
            list.addAll(gnsearcher.search(search, i));
            list.addAll(gnrsssearcher.search(search));
            list.addAll(ynsearcher.search(search, i));
            list.addAll(ysearcher.search(search));
            list.addAll(gcsearcher.search(search));
            list.addAll(gsearcher.search(search));
        }

        for (Object obj : list) {
            System.out.println(obj);
        }

        Assertions.assertTrue(list.size() > 0);
        System.out.println(list.size());

    }
}
