package etc;

import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


//FIXME Needs a lot of improvements in parsing

public class WebDateParser {
    // Google RSS News

    //"Thu, 02 Apr 2020 07:00:00 GMT"
    private static final SimpleDateFormat googleRssNewsDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);

    // Google News
    static final Pattern p = Pattern.compile("(\\d+)\\s+(.*?)s? ago");

    static final Map<String, Integer> fields = new HashMap<>() {{
        put("second", Calendar.SECOND);
        put("minute", Calendar.MINUTE);
        put("hour", Calendar.HOUR);
        put("day", Calendar.DATE);
        put("week", Calendar.WEEK_OF_YEAR);
        put("month", Calendar.MONTH);
        put("year", Calendar.YEAR);
    }};

    // Yandex News
    private static final SimpleDateFormat yandexNewsDateFormat = new SimpleDateFormat("dd MMM в HH:mm", new Locale("ru"));
    private static final SimpleDateFormat yandexNewsDateFormat2 = new SimpleDateFormat("dd.MM.yyyy в HH:mm", new Locale("ru"));
    private static final SimpleDateFormat yandexNewsTimeFormat = new SimpleDateFormat("HH:mm", new Locale("ru"));


    static public Date parseGoogleNews(String s) {

        if(s == null){
            return null;
        }

        Matcher m = p.matcher(s);
        Date ret = new Date(-1);
        try {
            if (m.matches()) {
                int amount = Integer.parseInt(m.group(1));
                String unit = m.group(2);

                Calendar cal = Calendar.getInstance();

                int field = fields.get(unit);
                if(field > 0) {
                    cal.add(field, -amount);
                }
                
                ret = cal.getTime();
            }
        } catch (Exception ignored) {

        }
        return ret;
    }
    static public Date parseGoogleRssNews(String s){
        if(s == null){
            return null;
        }

        try {
            return googleRssNewsDateFormat.parse(s);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    static public Date parseYandexNews(String s){
        if(s == null){
            return null;
        }

        Calendar ret = Calendar.getInstance();

        try{
            ret.setTime(yandexNewsDateFormat.parse(s));
        }catch (Exception e){
            try{
                ret.setTime(yandexNewsDateFormat2.parse(s));
            }catch (Exception e1){
                try {
                    final String yesterday = "вчера в ";
                    if (s.contains(yesterday)) {
                        s = s.replaceAll(yesterday, "");
                        ret.add(Calendar.DATE, -1);
                    }
                    Calendar time = Calendar.getInstance();
                    time.setTime(yandexNewsTimeFormat.parse(s));

                    ret.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
                }catch (Exception e2){
                    e2.printStackTrace();
                }
            }
        }
        if(ret.get(Calendar.YEAR)==1970){
            ret.set(Calendar.YEAR, Year.now().getValue());
        }
        return ret.getTime();
    }
}
