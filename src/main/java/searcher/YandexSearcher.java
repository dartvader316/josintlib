package searcher;

import etc.WebUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import search.Search;
import searchHelper.SearchHelper;


import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;



public class YandexSearcher extends AbstractSearcher<Search>{
    int searchSize = -1;


    public static enum YANDEX_SEARCH_SUPPORTED_SIZE {
        // Yandex supports only this numbers
        SIZE_10(10),
        SIZE_20(20),
        SIZE_30(30),
        SIZE_50(50);

        private final int value;

        public int getValue() {
            return value;
        }

        YANDEX_SEARCH_SUPPORTED_SIZE(final int newValue) {
            value = newValue;
        }
    }


    public YandexSearcher(SearchHelper searchHelper) {
        super(searchHelper);
    }


    public void setSearchSize(YANDEX_SEARCH_SUPPORTED_SIZE size) {
        this.searchSize = size.getValue();
    }

    private List<Search> parse(String html) throws Exception {
        /*
        <h2 class="OrganicTitle Typo Typo_text_l Typo_line_m organic__title-wrapper">
            <a class="Link Link_theme_normal OrganicTitle-Link OrganicTitle-Link_wrap organic__url link"
                tabindex="0" target="_blank"
                data-counter="[&quot;b&quot;]"
                data-log-node="b0jmw0k0"
                href="%LINK%">

                  <div class="Favicon Favicon_size_m">
                   <div class="Favicon-Icon Favicon-Page0" style="background-position-y:-112px;width:16px;height:16px"></div>
                  </div>
                  <div class="OrganicTitle-LinkText organic__url-text">
                  </div>
             </a>
         </h2>
         */
        /*FIXME
            dublicates in parsing
            Temporary fixed by using Set
         */

        Set<Search> result = new LinkedHashSet<>();
        html = WebUtils.normalizeHtml(html);
        Document doc = Jsoup.parse(html);

        Elements results = doc.select("h2 > a > div");
        for (Element link : results) {
            if(link.hasParent()) {
                Elements parent = link.parent().getAllElements();
                String relHref = parent.attr("href");

                result.add(new Search(relHref));
            }
        }
        return new ArrayList<>(result);
    }


    private String searchYandexQuery(String yandexSearchQuery) throws Exception {

        if(searchSize != -1) {
            yandexSearchQuery += "&numdoc=" + searchSize;
        }

        String queryUrl = "https://yandex.ru/yandsearch?text=" + yandexSearchQuery;

        URLConnection connection = connect(queryUrl);

        if(connection != null) {
            return WebUtils.readHTML(connection);
        }

        throw new IOException("Cant connect to " + queryUrl);
    }


    @Override
    public List<Search> search(String s) throws Exception {
        return parse(searchYandexQuery(s));
    }

}
