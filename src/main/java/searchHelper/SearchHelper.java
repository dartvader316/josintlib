package searchHelper;

import etc.WebUtils;

import java.io.IOException;
import java.net.URLConnection;

abstract public class SearchHelper {
    static protected void appendRequestProperty(URLConnection connection,String field,String appender){
        String cookies = connection.getRequestProperty(field);
        if(cookies != null){
            if (!cookies.endsWith(";")) {
                cookies += ";";
            }
        }else{
            cookies = "";
        }
        cookies += appender;
        connection.setRequestProperty(field,cookies);
    }
    public URLConnection connect(String url) throws IOException {
        return WebUtils.connect(url);
    }
}
