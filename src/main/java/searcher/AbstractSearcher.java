package searcher;

import etc.WebUtils;
import search.Search;
import searchHelper.SearchHelper;

import java.io.IOException;
import java.net.URLConnection;
import java.util.List;

abstract public class AbstractSearcher<RT extends Search> {
    SearchHelper searchHelper = null;

    public AbstractSearcher(SearchHelper searchHelper) {
        this.searchHelper = searchHelper;
    }

    public URLConnection connect(String url) throws IOException {
        if(searchHelper != null){
            return this.searchHelper.connect(url);
        }

        return WebUtils.connect(url);
    }
    abstract public List<RT> search(String s) throws Exception;
}
