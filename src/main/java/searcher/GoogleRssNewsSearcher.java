package searcher;

import etc.CyrillicStringUtils;
import etc.WebDateParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import search.NewsSearch;
import searchHelper.SearchHelper;

import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import static etc.WebUtils.normalizeHtml;
import static etc.WebUtils.readHTML;


public class GoogleRssNewsSearcher extends AbstractSearcher<NewsSearch>{
    int searchSize = -1;

    public enum googleNewsLanguage {
        RUSSIAN("ru","RU","RU:ru");
        String hl;
        String gl;
        String ceid;

        googleNewsLanguage(String hl, String gl, String ceid) {
            this.hl = hl;
            this.gl = gl;
            this.ceid = ceid;
        }

        @Override
        public String toString() {
            return "hl=" + hl
                    + "&gl=" + gl
                    + "&ceid=" + ceid;
        }
    }
    googleNewsLanguage language = null;


    public GoogleRssNewsSearcher(SearchHelper searchHelper) {
        super(searchHelper);
    }

    public void setLanguage(googleNewsLanguage language) {
        this.language = language;
    }

    public void setSearchSize(int searchSize) {
        this.searchSize = searchSize;
    }



    private List<NewsSearch> parse(String html){
        List<NewsSearch> result = new ArrayList<>();
        html = normalizeHtml(html);
        Document doc = Jsoup.parse(html);

        Elements results = doc.select("item");
        for (Element item : results) {
            final String[] cutStrings = {" - "," | "};

            String title = item.select("title").first().ownText();
            int cutIndex = -1;
            for(String s : cutStrings){
                cutIndex = title.lastIndexOf(s);
                if(cutIndex>0) break;
            }

            if(cutIndex>0) {
                title = title.substring(0, cutIndex);
            }

            result.add(new NewsSearch(
                    title,
                    item.select("link").first().nextSibling().toString(),
                    WebDateParser.parseGoogleRssNews(item.select("pubDate").first().ownText()),
                    item.select("description").first().wholeText()
                    ));
        }
        return result;
    }

    private String searchGoogleRssNewsQuery(String query) throws Exception{

        //FIXME only fixes cyrillic chars
        if(CyrillicStringUtils.hasCyrillicChar(query)) {
            query = CyrillicStringUtils.toLatin(query);
        }

        query += "+after:2019-1-1";

        if(language != null){
            query += "&" + language;
        }

        if(searchSize != -1) {
            query += "&num=" + searchSize;
        }

        String queryUrl = "https://news.google.com/rss/search?cf=all&q=" + query;

        URLConnection connection = connect(queryUrl);

        if(connection != null) {
            return readHTML(connection);
        }

        throw new IOException("Cant connect to " + queryUrl);
    }

    @Override
    public List<NewsSearch> search(String s) throws Exception{
        return parse(searchGoogleRssNewsQuery(s));
    }

}
