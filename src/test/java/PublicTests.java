import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import searchHelper.DebugSearchHelper;

public class PublicTests {

    @Test
    @DisplayName("Search test")
    void searchTest() throws Exception {
        CommonTests.searchTest(new DebugSearchHelper());
    }
}
