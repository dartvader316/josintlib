package searcher;

import etc.WebUtils;
import etc.WebDateParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import search.NewsSearch;
import searchHelper.SearchHelper;

import java.util.ArrayList;
import java.util.List;

public class GoogleNewsSearcher extends AbstractGoogleSearcher<NewsSearch> {

    public GoogleNewsSearcher(SearchHelper searchHelper) {
        super(searchHelper);
    }

    @Override
    protected List<NewsSearch> parse(String html) throws Exception {
        List<NewsSearch> result = new ArrayList<>();
        html = WebUtils.normalizeHtml(html);
        Document doc = Jsoup.parse(html);

        Elements results = doc.select("a > h3");

        for (Element item : results) {
            if(item.hasParent()) {
                Elements parent = item.parent().getAllElements();

                String relHref = parent.attr("href");
                String title = parent.select("h3 > div").first().ownText();
                Element timeEl = parent.parents().select("div > span").parents().first();
                String[] buffer = timeEl.wholeText().split(" ·");

                String time = "";
                String description = "";
                if(buffer.length==2){
                    time = buffer[0];
                    description = buffer[1];
                }


                if (relHref.startsWith("/url?q=")) {
                    relHref = relHref.replace("/url?q=", "");
                }
                String[] splittedString = relHref.split("&sa=");
                if (splittedString.length > 1) {
                    relHref = splittedString[0];
                }
                result.add(new NewsSearch(title,relHref, WebDateParser.parseGoogleNews(time),description));
            }
        }
        return result;
    }

    @Override
    protected String searchGoogleQuery(String query, int page) throws Exception {
        return super.searchGoogleQuery(query + "&source=lnms&tbm=nws&lr=lang_ru",page);

    }

}
