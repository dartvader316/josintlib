package search;


public class GoogleSearch extends Search{
    String title;
    public GoogleSearch(String title,String link) {
        super(link);
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "GoogleSearch{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
