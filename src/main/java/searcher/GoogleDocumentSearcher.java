package searcher;


import search.GoogleSearch;
import searchHelper.SearchHelper;

import java.util.List;

public class GoogleDocumentSearcher extends GoogleSearcher{
    final String query = "+filetype:pdf+OR+filetype:docx+OR+filetype:xlsx+OR+filetype:pptx";

    public GoogleDocumentSearcher(SearchHelper searchHelper) {
        super(searchHelper);
    }

    @Override
    public List<GoogleSearch> search(String s) throws Exception {
        return this.search(s,0);
    }
    @Override
    public List<GoogleSearch> search(String s,int page) throws Exception {
        return super.search(s + query,page);
    }

}
