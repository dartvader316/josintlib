package searcher;

import etc.WebUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import search.GoogleSearch;
import searchHelper.SearchHelper;

import java.util.ArrayList;
import java.util.List;


public class GoogleSearcher extends AbstractGoogleSearcher<GoogleSearch> {

    public GoogleSearcher(SearchHelper searchHelper) {
        super(searchHelper);
    }

    protected List<GoogleSearch> parse(String html) throws Exception {

        List<GoogleSearch> result = new ArrayList<>();
        html = WebUtils.normalizeHtml(html);
        Document doc = Jsoup.parse(html);

        Elements results = doc.select("a > h3");

        for (Element link : results) {
            if(link.hasParent()) {

                Elements parent = link.parent().getAllElements();
                String relHref = parent.attr("href");
                String title = parent.select("h3 > div").first().ownText();
                if (relHref.startsWith("/url?q=")) {
                    relHref = relHref.replace("/url?q=", "");
                }
                String[] splittedString = relHref.split("&sa=");
                if (splittedString.length > 1) {
                    relHref = splittedString[0];
                }
                result.add(new GoogleSearch(title, relHref));

            }
        }

        return result;
    }




}
