package search;

public class Search{
    String link;

    public Search(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String toString() {
        return "Search{" +
                "link='" + link + '\'' +
                '}';
    }
}
