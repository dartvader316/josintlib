package search;

import java.util.Date;

public class NewsSearch extends Search {
    String title;
    Date pubDate;
    String description;

    public NewsSearch(String title, String link, Date pubDate, String description) {
        super(link);
        this.title = title;
        this.pubDate = pubDate;
        this.description = description;
    }

    @Override
    public String toString() {
        return "NewsSearch{" +
                "title='" + title + '\'' +
                ", pubDate=" + pubDate +
                ", description='" + description + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
