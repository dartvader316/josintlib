package searcher;


import search.GoogleSearch;
import searchHelper.SearchHelper;

import java.util.List;


public class GoogleCompanySearcher extends GoogleSearcher{

    public GoogleCompanySearcher(SearchHelper searchHelper) {
        super(searchHelper);
    }

    @Override
    public List<GoogleSearch> search(String s,int page) throws Exception {
        return super.search(s + "+site:www.list-org.com",page);
    }

    @Override
    public List<GoogleSearch> search(String s) throws Exception {
        return this.search(s + "+site:www.list-org.com",0);
    }

}
