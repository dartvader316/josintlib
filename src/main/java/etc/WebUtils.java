package etc;
import org.apache.commons.text.StringEscapeUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class WebUtils {
    public static String normalizeHtml(String html){
        return StringEscapeUtils.unescapeHtml4(html);
    }

    protected static String getHtmlString(InputStream is) {
        StringBuilder sb = new StringBuilder();

        String line;
        try(BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //System.out.println(sb.toString());
        return sb.toString();
    }
    public static URLConnection connect(String link) throws IOException {

        URL url = new URL(link);
        URLConnection connection = url.openConnection();

        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        return connection;
    }

    public static String readHTML(URLConnection connection) throws IOException {
        final InputStream stream = connection.getInputStream();
        return getHtmlString(stream);
    }

    public static String fixEncoding(String str){
        try {
            return URLEncoder.encode(str.trim(), StandardCharsets.UTF_8.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
