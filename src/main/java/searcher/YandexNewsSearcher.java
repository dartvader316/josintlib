package searcher;

import etc.WebDateParser;
import etc.WebUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import search.NewsSearch;
import searchHelper.SearchHelper;

import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


public class YandexNewsSearcher extends AbstractSearcher<NewsSearch> implements PageSearcher<NewsSearch> {
    int searchSize = -1;

    public YandexNewsSearcher(SearchHelper searchHelper) {
        super(searchHelper);
    }

    private List<NewsSearch> parse(String html) throws Exception {
        Set<NewsSearch> result = new LinkedHashSet<>();
        html = WebUtils.normalizeHtml(html);
        Document doc = Jsoup.parse(html);
        Elements results = doc.select("article");
        for (Element item : results) {
                String link  = null;
                String title = null;
                String date = null;
                String desc = null;

                Element linkEl = item.select(".mg-snippet__url").first();
                Element titleEl = item.select(".mg-snippet__title").first();
                Element dateEl = item.select(".news-snippet-source-info__time").first();
                Elements descEls = item.select(".mg-snippet__text");

                if(linkEl != null){
                   link =  linkEl.attr("href");
                    String[] splittedString = link.split("\\?utm_source=yxnews");
                    if (splittedString.length > 1) {
                        link = splittedString[0];
                    }
                }

                if(titleEl != null){
                    title = titleEl.wholeText();
                }
                if(dateEl != null){
                    date = dateEl.ownText();
                }

                if (descEls != null && descEls.size() > 0) {
                    desc = descEls.first().wholeText();
                }

                result.add(new NewsSearch(title, link, WebDateParser.parseYandexNews(date), desc));
        }

        return new ArrayList<>(result);
    }

    private String searchYandexQuery(String query,int page) throws Exception {

        if(searchSize != -1) {
            query += "&numdoc=" + searchSize;
        }
        query += "&p=" + page;
        query += "&flat=1";

        String queryUrl = "https://newssearch.yandex.ru/news/search?text=" + query;

        URLConnection connection = connect(queryUrl);

        if(connection != null) {
            return WebUtils.readHTML(connection);
        }

        throw new IOException("Cant connect to " + queryUrl);
    }

    @Override
    public List<NewsSearch> search(String s) throws Exception {
        return parse(searchYandexQuery(s,0));
    }


    @Override
    public List<NewsSearch> search(String s, int page) throws Exception {
        return parse(searchYandexQuery(s,page));
    }

}
