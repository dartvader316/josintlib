package searchHelper;

import java.io.IOException;
import java.net.URLConnection;

public class DebugSearchHelper extends SearchHelper{
    @Override
    public URLConnection connect(String url) throws IOException {
        System.out.println("DebugSearchHelper: connecting to " + url);
        return super.connect(url);
    }
}
