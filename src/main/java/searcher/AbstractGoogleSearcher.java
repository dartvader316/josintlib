package searcher;

import etc.WebUtils;
import search.Search;
import searchHelper.SearchHelper;

import java.net.URLConnection;
import java.util.List;

public abstract class AbstractGoogleSearcher <T extends Search> extends AbstractSearcher<T> implements PageSearcher<T> {
    int searchSize = -1;

    public AbstractGoogleSearcher(SearchHelper searchHelper) {
        super(searchHelper);
    }
    public void setSearchSize(int searchSize) {
        this.searchSize = searchSize;
    }

    abstract protected List<T> parse(String html) throws Exception;

    protected String searchGoogleQuery(String query,int page) throws Exception {

        if(searchSize != -1) {
            query += "&num=" + searchSize;
        }
        query += "&start=" + page * (searchSize == -1 ? 10 : searchSize);

        String queryUrl = "https://www.google.com/search?q=" + query;

        URLConnection connection = connect(queryUrl);

        if(connection != null) {
            return WebUtils.readHTML(connection);
        }
        throw new Exception("Cant connect to " + queryUrl);
    }
    @Override
    public List<T> search(String s) throws Exception {
        return parse(searchGoogleQuery(s,0));
    }

    @Override
    public List<T> search(String s, int page) throws Exception {
        return parse(searchGoogleQuery(s,page));
    }
}
